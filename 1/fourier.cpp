/*
fourier.cpp:
(1)のデータを離散フーリエ変換するコード
*/
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cmath>
#include <complex>
#include <cstdlib>

using namespace std;
typedef std::complex<double> comp;

//define constants
  static const int M = 4096;             //the number of data
  static const double T = 31.4159; //周期
  static const double pi = 4.0*atan(1.0);
  const comp RUNIT(1.0, 0.0);
  const comp IUNIT(0.0, 1.0);

int main(){

//define variables
  int k,n;
  double x[M];
  double f[M];
  comp F[M];    

//set data files
  FILE *function;
  FILE *fourier;
  function=fopen("reportdata.txt","r");
  fourier=fopen("spectrum.dat","w");

//output a warning when the input data cannot be opened
  if(function == NULL){
     printf("you cannot open input data file!!\n");
     return 1;
  }

//read the input data
  for(n=0;n<M;n++) {
     fscanf(function,"%lf	%lf",&x[n], &f[n]);
  }



//calculate fourier translation
  for(n=0;n<M;n++){
        double N=(n-M/2)*1.0;
        double m=M*1.0;//実際のフーリエ変換計算で型を揃えるおまじない。
        F[n] = 0.0;
     for(k=0;k<M;k++){
	double l = k*1.0;//つぎの行の計算でkの型を他のと揃えるおまじない。
        F[n] = F[n] + f[k]*exp(-2.0*pi*IUNIT*l*N/m);
     }
     //write output data
     fprintf(fourier,"%f %f %f %f %f\n",N, N/T, real(F[n]),imag(F[n]),abs(F[n])*abs(F[n]));  //2列目は講義ノートNo.12の式(6.69)を用いて[-\pi/\tau:\pi/\tau]の区間になるように周波数の調整をした。
  }

  fclose(function);
  fclose(fourier);
        
  return 0;
}
