# FFT #

電子回路の最終レポートの離散フーリエ変換の問題のコード置き場.何かあればpull request等お願いします.

### test###

* 定数関数を入力関数としたFFT.一応きちんとデルタ関数(の正の部分)が出力されるようになっているはず.

### 1 ###
(1)の数値計算. このコードではFFTではなく離散フーリエ変換を利用した.

* reportdata.txt:レポート問題にある入力の部分.詳しくは[reportdata.txt](http://kats.issp.u-tokyo.ac.jp/kats/electroniccircuit/report/reportdata.txt)を見よ.
* fourier.cpp:コード.離散フーリエ変換だと等間隔で切られることで時間情報に対応する整数が入ることを用いて修正.
* spectrum.dat:出力の部分。1列目がデータ番号, 2列目が周波数[Hz], 3,4列目がそれぞれFFT後の実成分と虚成分, 5列目がパワースペクトル.
* spectrum.eps:spectrum.dat(このepsの.datはもうない)を横軸周波数, 縦軸パワースペクトルにしてプロットしたもの. 時間情報をそのまま肩に乗っけた計算結果.たぶん間違い.
* spectrumnew.eps:spectrum.datを横軸2列目, 縦軸5列目のデータを用いてspectrum.epsと同じ感じでプロットしたもの.きちんとピークが出ている！やったね！！

### 2 ###
(2)の数値計算. このコードでもFFTではなく離散フーリエ変換を利用した.

* reportdata.txt:レポート問題にある入力の部分.詳しくは[reportdata.txt](http://kats.issp.u-tokyo.ac.jp/kats/electroniccircuit/report/reportdata.txt)を見よ.
* window.cpp:コード.基本は(1)と同じ.ただし, 窓のサイズ及び位置を決められるようにした.
* window.dat:出力の部分。1列目がデータ番号, 2列目がx, 3,4列目がそれぞれFFT後の実成分と虚成分, 5列目が周波数振幅.
* window.eps:window.datを横軸2列目, 縦軸5列目のデータを用いてspectrumnew.epsと同じ感じでプロットしたもの.
