#include <iostream>
#include <fstream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <complex>
#include <cstdlib>

using namespace std;
typedef std::complex<double> comp;

//define constants
  static const int M =  10001;             //the number of data
  static const double pi = 4.0*atan(1.0);  
  const comp RUNIT(1.0, 0.0);
  const comp IUNIT(0.0, 1.0);

int main(){

//define variables
  int k,n,N;
  double x[M];
  double f[M];
  comp F[M];    
  double dx = 0.01;    

//set data files
  FILE *fourier;
  fourier=fopen("test2.dat","w");
 
  for(N=0;N<M;N++){
     x[N]=N*dx-50.0;
     f[N]=1.0;
  }

/*  for(N=4000;N<6000;N++){
     x[N]=N*dx-50.0;
     f[N]=1.0;
  }*/

//calculate fourier translation
  for(n=0;n<N;n++){
        double j=n*1.0;
        double i=N*1.0;
        F[n] = 0.0;
     for(k=0;k<N;k++){
        F[n] = F[n] + f[k]*exp(2.0*pi*IUNIT*x[k]*j/i);
     }
     //write output data
     fprintf(fourier,"%d %f %f %f\n",n,real(F[n]),imag(F[n]),abs(F[n])*abs(F[n]));   
  }


  fclose(fourier);
        
  return 0;
}
