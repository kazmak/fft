/*
window.cpp:
(2)の窓付き離散フーリエ変換のコード
*/
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cmath>
#include <complex>
#include <cstdlib>

using namespace std;
typedef std::complex<double> comp;

//define constants
  static const int M = 4096;             //the number of data
  static const int W = 256;  //the width of the window
  static const double T = 31.4159; //周期
  static const double pi = 4.0*atan(1.0);
  const comp RUNIT(1.0, 0.0);
  const comp IUNIT(0.0, 1.0);

int main(){

//define variables
  int k,l,n;
  double x[M];
  double f[M];
  comp F[M];    

//set data files
  FILE *function;
  FILE *window;
  function=fopen("reportdata.txt","r");
  window=fopen("window.dat","w");

//output a warning when the input data cannot be opened
  if(function == NULL){
     printf("you cannot open input data file!!\n");
     return 1;
  }

//read the input data
  for(n=0;n<M;n++) {
     fscanf(function,"%lf	%lf",&x[n], &f[n]);
  }



//calculate fourier translation
  //define the position of the window
  k=0;
  //calculate FT in the window
  while(k<M-W){
        double m=M*1.0;//実際のフーリエ変換計算で型を揃えるおまじない。
        F[400] = 0.0;
	for(l=k;l<k+W+1;l++){
	  double L = l*1.0;//つぎの行の計算でkの型を他のと揃えるおまじない。
          F[400] = F[400] + f[l]*exp(-2.0*pi*IUNIT*L*400.0/m);
	}
     //write output data
        fprintf(window,"%d %f %f %f %f\n", k, x[k], real(F[400]),imag(F[400]),abs(F[400])); 
       	k += 100;
  }
     
  fclose(function);
  fclose(window);
        
  return 0;
}
